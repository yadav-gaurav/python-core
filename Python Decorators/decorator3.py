def decorator_wish(func):
    def inner(name):
        if name == 'sunny leone':
            print('*'*10)
            print('Hey you are very important for my life pls do not go')
            print('Good morning very very to you ', name)
            print('*'*10)
        else:
            func(name)
    return inner


@decorator_wish
def wish(name):
    print('Hey Good Morning, have a nice day !!', name)


wish('Yadav gaurav')
wish('sunny leone')
