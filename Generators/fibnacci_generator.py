def fibnacc_generator(number):
    a, b = 0, 1
    while b <= number:
        yield a
        a, b = b, a+b


generator = fibnacc_generator(21)
for num in generator:
    print(num)
