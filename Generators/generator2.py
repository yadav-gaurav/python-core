def mygen():
    yield 'A'
    yield 'B'
    yield 'C'


g = mygen()
print('Type of mygen() :', type(g))

for data in g:
    print(data)

print('*************************************')
# WAF to generate first n number.


def n_numbers_generator(number):
    i = 1
    while i <= number:
        yield i
        i = i+1


generator_obj = n_numbers_generator(20)
print('type of function : ', type(generator_obj))
for num in generator_obj:
    print(num)
