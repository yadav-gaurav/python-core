# Dynamic creation
print('Dynamic creation')
l = list()
print(type(l))

# convert string to list:
print('convert string to list:')
name = 'yadav'
name_list = list(name)
print(name_list[1])
print(name_list)
print(type(name_list))


# create list with range function
number_list = list(range(0, 10, 2))
print("Even number list :", number_list)

# split function
cast = 'Yadav is not good guys'
cast_list = cast.split()
print(cast_list)
