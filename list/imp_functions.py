# Membership operator in & not in
name = 'yadavgaurav'
name_list = list(name)
name_dict = dict()
for char in name_list:
    char_count = name_list.count(char)
    if char not in name_dict:
        name_dict[char] = char_count
print(name_dict)

# index()
l = [1, 2, 3, 45, 5]
x = int(input('Enter an number!!!'))
if x in l:
    print('{} is at index of {}'.format(x, l.index(x)))

else:
    print('{} is not available in the list'.format(x))


# append()
number_list = [1, 2, 3]
num = input('Enter a number')
number_list.append(num)
print(number_list)


# insert()
# Add element at index of 2 in number_list
new_number = int(input("Enter new number"))
number_list.insert(2, new_number)
print(number_list, number_list.index(new_number))
