import time


class Test:
    def __init__(self):
        print('perform intialization of instance variable ...')

    # destructor
    def __del__(self):
        print('perform the clean up activities ....')


t1 = Test()
t2 = Test()
print('End of application...')

time.sleep(10)
