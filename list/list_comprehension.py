# creating list with range of 1 to 10.
# using normal way
l = []
for num in range(1, 11):
    l.append(num)
print('List with normal way :', l)

# list with comprehension way
number_list = [num for num in range(1, 11)]
print('list with comprehension way :', number_list)

# odd number list
odd_number_list = [num for num in range(1, 11) if num % 2 == 1]
print(odd_number_list)
