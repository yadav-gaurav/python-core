# IS-A is called as Inheritance
class Parent:
    def m1(self):
        print('I am from parent class')


class Child(Parent):
    def m2(self):
        print('I am from child class..')


child_obj = Child()
child_obj.m2()
child_obj.m1()
