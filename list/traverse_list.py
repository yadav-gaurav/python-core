number_list = [1, 2, 3, 4, 5]

# using while loop
index = 0
while index < len(number_list):
    print(number_list[index])
    index = index+1

# using for loop
for number in number_list:
    print(number)
