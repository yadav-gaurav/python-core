# sort() - applicable ony for the list
# sort asscending order
number_list = [1, 5, 2, 6, 2]
print('Before sorting : ', number_list)
number_list.sort()
print('after sorting :', number_list)

# sort descending order
number_list.sort(reverse=True)
print('Print reverse sorting order ', number_list)

# sorted() -applicable to all not list specific
# return new list
name = 'yadav'
name_list = sorted(name)
print(name_list)
