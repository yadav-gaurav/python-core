# Inner class used for the security purpose.
class Outer:
    def __init__(self):
        print('Outer Constructor ..')

    def m1(self):
        print('Outer instance method ...')

    class Inner:
        def __init__(self):
            print('Inner class construnctor ..')

        def m2(self):
            print('Innner class instance method...')


# Outer class object
outer_obj = Outer()
outer_obj.m1()

# Inner class obj
inner_obj = outer_obj.Inner()
inner_obj.m2()
