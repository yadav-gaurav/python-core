# packing and unpacking applicable to all data structure

# packing
a = 1
b = 3
c = 4
d = 6
number_tuple = a, b, c, d
print('Packing :', number_tuple)

# unpacking
e, f, g, h = number_tuple
print('e is {},f is {}, g is {},h is {} :!!!!!!!!'.format(e, f, g, h))


# unpacking in less variables
p, q, *r = number_tuple
print('p is {}, q is {}, r is {}'.format(p, q, r))
