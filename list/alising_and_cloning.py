number_list = [1, 2, 3, 4, 5]
# aliasig
new_number_list = number_list
print(number_list, new_number_list)
print(number_list is new_number_list)
# cloning the list
# slice operator & copy method
print('copy list using slice operator : !')
new_copy_list1 = number_list[::]
print('original list :', id(number_list))
print('new copy of list :', id(new_copy_list1))
print(number_list is new_copy_list1)

print('copy the list using copy() !')
new_copy_list2 = number_list.copy()
print(new_copy_list2)
print(id(number_list), id(new_copy_list1), id(new_copy_list2))
