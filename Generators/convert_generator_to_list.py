def n_numbers_generator(number):
    i = 1
    while i <= number:
        yield i
        i = i+1


generator_obj = n_numbers_generator(20)
number_list = list(generator_obj)
print('type of function : ', type(generator_obj))
# convert generator to list:
print('Convert generator to list : **********')
print('converted generator to list : ', number_list)
