# write program to diplay unique vowels from given string
name_string = input('Enter string input !')
vowels = ['a', 'i', 'u', 'e', 'o']
output = {vowel for vowel in vowels if vowel in name_string}
print(output)


# second way
name_string_set = set(name_string)
print(name_string_set & set(vowels))
