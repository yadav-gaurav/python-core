# concatenation(+) & repetition (*)
t1 = (1, 2, 3)
t2 = (4, 5, 6)
t3 = t1+t2
print('concatenated tuple t3 :', t3)

# repetition operator
t4 = (1, 2, 3, 4)
t5 = t4*3
print('repeated operator :', t5)
