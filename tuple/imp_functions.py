# len() -> python inbuild function
number_tuple = 1, 2, 3, 4
print('length of tuple', len(number_tuple))

# count() -> python inbuild function
number_tuple1 = (1, 2, 3, 4, 3, 2, 2)
print("number of times 2 is here ", number_tuple1.count(2))


# index()->returns index of first occurence -> python inbuild function

print('index of 2 :', number_tuple1.index(2))


# reverse() not applicable in tuple

# reversed() applicable -> python inbuild function
number_tuple3 = (1, 2, 3, 4)
r = reversed(number_tuple3)
reversed_tuple = tuple(r)
print('Original tuple :', number_tuple3)
print('Reversed tuple is : ', reversed_tuple)


# sort() not applicable
# sorted() applicable -> python inbuild function
number_tuple4 = (1, 8, 3, 9, 2)
number_list = sorted(number_tuple4)
sorted_tuple = tuple(number_list)
print('Original tuple :', number_tuple4)
print('Sorted tuple :', sorted_tuple)


# max and min function -> python inbuild function
number_tuple5 = (1, 3, 2, 9, 5, 8)
print('max element in tuple', max(number_tuple5))

number_list2 = [1, 3, 2, 6, 5]
print('max element :', max(number_list2))
print('min element :', min(number_list2))


# append(),insert(),extend(),remove(),clear(),pop()-> not applicable in tuple
