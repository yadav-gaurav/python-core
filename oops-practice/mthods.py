class Student:
    """Student Information..."""
    school_name = 'B N B i College'

    def __init__(self, name, roll_number):
        self.name = name
        self.roll_number = roll_number

    def get_student_info(self):
        """instance method"""
        print('Student name : ', self.name)
        print('Student Roll Number :', self.roll_number)

    @classmethod
    def display_school_name(cls):
        """class methods"""
        print('School name of Student: ', cls.school_name)

    @staticmethod
    def display_age(age):
        """static method"""
        print('Age of Student : ', age)


student_obj = Student('Yadav Gaurav', '1234567')
# access all instance variables
print(student_obj.__dict__)

# add instance variable from outside
student_obj.age = 23
print(student_obj.__dict__)
