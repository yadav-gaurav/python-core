class Test:
    def m1(self, value):
        print('class name of value : {}'.format(value.__class__.__name__))
obj_test = Test()
obj_test.m1(10)
obj_test.m1(10.5)
obj_test.m1(True)