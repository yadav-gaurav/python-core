# Set the value of instance vatiable and get value of instance variable
# Generlly used when you want set dyanmic data.

class Student:
    def setName(self, name):
        self.name = name

    def getName(self):
        return self.name

    def setAge(self, age):
        self.age = age

    def getAge(self):
        return self.age


number_of_students = int(input('Enter the number of students : '))
list_of_students = []

for i in range(number_of_students):
    s = Student()
    name = input('Enter Student name :')
    age = input('Enter age of student :')
    s.setName(name)
    s.setAge(age)
    list_of_students.append(s)
for student in list_of_students:
    print('Name of Student : ', student.getName())
    print('Age of Student : ', student.getAge())
    print('**************', end='\n')
