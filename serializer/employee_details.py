class Employee:
    """Serialize multiple employee objects"""

    def __init__(self, name, age, salary):
        self.name = name
        self.age = age
        self.salary = salary

    def display(self):
        print('Employee name: {}, Employee age: {}, Employee salary: {}'.format(
            self.name, self.age, self.salary))
