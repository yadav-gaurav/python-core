number_list = [1, 2, 3, 4, 5]
# access list with positive index
print(number_list[2])

# access list with -ve index
print(number_list[-1])

# access list using slice operator.
number = [1, 2, 3, 34, 45, 23, 43, 65, 46, 98, 90]
print("copy in forward dirction..")
copy_list = number[0:5]
print(copy_list, number)
print(number[0:5:2])
print(number[3::2])
print("copy in backword diraction...")
print(number[8:2:-2])
print(number[8::-2])
