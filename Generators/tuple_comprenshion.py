number_tuple = (num for num in range(1, 101) if num % 3 == 0)
print('Type of number_tuple :', type(number_tuple))

for number in number_tuple:
    print(number)
