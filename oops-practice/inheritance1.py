class Person:
    def __init__(self, name, age):
        self.name = name
        self.age = age

    def eat_drink(self):
        print('Eating biryani and drinking beer..')


class Employee(Person):
    def __init__(self, name, age, eno, esal):
        super().__init__(name, age)
        self.eno = eno
        self.esal = esal

    def work(self):
        print('Do coding only all time...')

    def employee_info(self):
        print('Employee name : {}'.format(self.name))
        print('Eployee age : {}'.format(self.age))
        print('Employee egno : {}'.format(self.eno))
        print('Employee salary : {}'.format(self.esal))


employee_obj = Employee('Yadav Gaurav', 23, '23213eweqw', '3232k')
employee_obj.employee_info()
