nested_list = [[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12]]
print('Display the list in matrix form : ')
for number_list in nested_list:
    for num in number_list:
        print(num, end=' ')
    print()
