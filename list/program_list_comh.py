square_list = [num*num for num in range(1, 11)]
print(square_list)

# 2 cube number list
cube_list = [2**num for num in range(1, 11)]
print('2 cube list : ', cube_list)

# create list with element present in l1 but not l2
l1 = [10, 20, 30, 40]
l2 = [30, 40, 50, 60]
number_list = [num for num in l1 if num not in l2]
print(number_list)

# create list with common element
common_number_list = [num for num in l1 if num in l2]
print(common_number_list)

# create list with first char
string_list = ['yadav', 'ashu', 'khushboo', 'devender', 'ravi']
first_char_list = [char[0] for char in string_list]
print(first_char_list)

# create list
string = 'the quick brown fox jumps over the lazy dog'
string_list = string.split()
custom_list = [[string.upper(), len(string)] for string in string_list]
print(custom_list)
