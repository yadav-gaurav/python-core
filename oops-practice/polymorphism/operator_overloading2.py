class Student:
    def __init__(self, num):
        self.number = num
        
    def __add__(self, other):
        """"magic methods"""
        total_sum = self.number + other.number
        return total_sum
ob1 = Student(100)
ob2 = Student(200)
print(ob1+ob2)