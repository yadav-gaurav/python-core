# reverse() - applicable only for list
number_list = [1, 2, 3, 4, 5]
print("Before reversing the list :", number_list)
number_list.reverse()
print('After reverse the list :', number_list)


# reversed()- applicable to list,set,tuple,..etc
list_odd_number = [1, 3, 5, 7, 9]
it = reversed(list_odd_number)
print('Iterator :', it)
list_odd = list(it)
print('list :', list_odd)

# eg.
name = 'yadav gaurav'  # iterator
name_rev = reversed(name)  # can't apply reverse() bcoz applicable only list.
for char in name_rev:
    print(char)
