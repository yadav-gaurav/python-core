# len() -> python in built method
number_set = {1, 2, 3}
print(len(number_set))

# add()
number_set.add(4)
print(number_set)

# update() -> add multiple objects to set.
number_set2 = set()
s1 = {1, 2, 3}
s2 = {5, 6, 7}
s3 = {0, 9}
number_set2.update(s1)
print(number_set2)
number_set2.update(s2, s3)
print(number_set2)


# sorted() -> python in built method
# return type list

random_number = {1, 8, 2, 4, 0, 7}
sorted_list = sorted(random_number)
print('Sorted list :', sorted_list)
print('sorted set :', set(sorted_list))
