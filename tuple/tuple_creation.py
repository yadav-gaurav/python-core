# Declare tuple with brakets
number_tuple = (1, 2, 3, 45)
print(number_tuple, type(number_tuple))

# Declare tuple without brakets
string_tuple = 'yadav', 'gaurav', 'ashu', 'khushboo'
print(string_tuple, type(string_tuple))

# create dynamic tuple
dynamic_tuple = tuple()
print(type(dynamic_tuple))


# convert string to tuple
name = "yadav gaurav"
print(tuple(name))

# try to change value at index
name_tuple = tuple(name)
print(name_tuple)
print(name_tuple[3])
# you can't chnage elements in tuple
# because it's immutable
name_tuple[3] = 'g'
print(name_tuple)
