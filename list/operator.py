# Concatenation(+) & Repetition operators(*)

# concatenation
l1 = [1, 2, 3, 4]
l2 = [5, 6]
l3 = l1+l2
print('concatenated list l3 :', l3)

# Repetition
l4 = [1, 3, 5]
l5 = l4*2
print('Repetition of list l4 :', l5)


# membership operator
print(1 in l1)
print('ram' in l1)
