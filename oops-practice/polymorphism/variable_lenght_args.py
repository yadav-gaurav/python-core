class Test:
    def m1(self, *args):
        print('args', args)
        total=sum(args)
        print('Sum is :',total)
        print("*" * 30)
obj = Test()
obj.m1(1,2,3,4,5)
obj.m1(1, 2, 3)
obj.m1(1)
obj.m1()