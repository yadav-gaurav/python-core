# instance,static,local

class Student:
    """Initialize student info and display info"""
    school_name = 'B N B Inter College'  # static variable

    def __init__(self, name, roll_number):
        self.name = name                      # instance variable
        self.roll_number = roll_number        # instance variable

    def display_info(self, flag):
        print("Student Information :", flag)
        print("Name : {}, Roll Number: {}, College : {}".format(
            self.name, self.roll_number, Student.school_name))


student_obj = Student("Yadav Gaurav", "123456789")
student_obj.display_info("Yes")
