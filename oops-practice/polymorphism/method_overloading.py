class Test:
    def m1(self):
        print('Zero argument method')
    def m2(self,arg1):
        print('One argument method')
    def m3(self, arg1, arg2):
        print("Two arguments method")
obj_test = Test()
obj_test.m1()
obj_test.m1(10)
obj_test.m1(10,20)