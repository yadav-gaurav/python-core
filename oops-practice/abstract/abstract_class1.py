from abc import ABC, abstractmethod

class Polygon(ABC):

    @abstractmethod
    def no_of_sides(self):
        pass

class Triangle(Polygon):
    def no_of_sides(self):
        print('I have 3 sides !!!!')

class Pentagon(Polygon):
    def no_of_sides(self):
        print('I have 5 sides !!!!')

class Hexagon(Polygon):
    def no_of_sides(self):
        print('I have 6 sides !!!!')

class Quadrilateral(Polygon):
    def no_of_sides(self):
        print('I have 4 sides !!!!')

t_obj = Triangle()
t_obj.no_of_sides()

p_obj = Pentagon()
p_obj.no_of_sides()

h_obj = Hexagon()
h_obj.no_of_sides()

q_obj = Quadrilateral()
q_obj.no_of_sides()