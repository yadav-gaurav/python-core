class P:
    a = 10

    def __init__(self):
        self.b = 20
        print('Parent class constructor ...')

    def m1(self):
        print('Parent class instance method..')

    @classmethod
    def m2(cls):
        print('Parent class class method')

    @staticmethod
    def m3():
        print('Parent class Static method..')


class C(P):
    pass


c_object = C()
c_object.m1()
c_object.m2()
c_object.m3()
