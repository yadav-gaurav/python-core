class Parent:
    a = 10

    def __init__(self):
        print('Parent constructor ...')

    def m1(self):
        print('Parent instance method')

    @classmethod
    def m2(cls):
        print('Parent class method..')

    @staticmethod
    def m3():
        print('Parent Static method ..')


class Child(Parent):
    def __init__(self):
        print('Child class constructor ...')

    def method(self):
        print('*'*30)
        print('Child class method calll')
        print('&'*30)
        print('Parent class static variable :', super().a)
        super().m1()
        super().m2()
        super().m3()
        super().__init__()


child_obj = Child()
child_obj.method()
