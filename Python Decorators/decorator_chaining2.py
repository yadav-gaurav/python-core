def decor1_num(func):
    def inner1():
        x = func()
        return x*x
    return inner1


def decor2_num(func):
    def inner2():
        x = func()
        return 2*x
    return inner2


@decor2_num
@decor1_num
def num():
    return 20


print(num())
