def decorator_add(func):
    def inner(a, b):
        print('*'*10)
        print('Sum of {} and {} :'.format(a, b), end=' ')
        func(a, b)
        print('*'*10)
    return inner


@decorator_add
def add_numbers(a, b):
    print(a+b)


add_numbers(10, 20)
