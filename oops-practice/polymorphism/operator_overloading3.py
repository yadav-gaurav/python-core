class Employee:
    def __init__(self, name, salary):
        self.name = name
        self.salary_per_day = salary
        
    def __mul__(self, other):
        """"magic method for overloading"""
        total_salary = self.salary_per_day * other.salary_per_day
        return total_salary
class Manager:
    def __init__(self, name, salary):
        self.name = name
        self.salary_per_day = salary
        
    def __mul__(self, other):
        """"magic method for overloading"""
        total_salary = self.salary_per_day * other.salary_per_day
        return total_salary
         
emp_obj = Employee('Ashu Yadav', 40000)
mag_obj = Manager('Yadav Gaurav', 900000000)
print('Operator overloading : ', emp_obj * mag_obj)

# but if you change order then you have to implement magic method in Manger
print('Operator overloading : ', mag_obj*emp_obj)
