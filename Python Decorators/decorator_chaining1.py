def decor1_display(func):
    def inner1():
        print('Decor1 function ')
        func()
    return inner1


def decor2_display(func):
    def inner2():
        print('decro2 function ..')
        func()
    return inner2


@decor2_display
@decor1_display
def display():
    print('Original function ..')


display()
