# static creation
number_tuple = (1, 2, 3)
print(number_tuple)

# Dyanmic tuple creation
number_list = [1, 2, 3, 4]
number_tuple1 = tuple(number_list)
print(number_tuple1)

number_range = tuple(range(1, 20, 2))
print(number_range)

name_string = 'yadavgaurav'
name_tuple = tuple(name_string)
print(name_tuple)

# read input as tuple from keyboard
input_tuple = eval(input('Enter your input !!!!'))  # not valid
print(input_tuple)
