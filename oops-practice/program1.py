class Test:
    a = 10

    def m1(self):
        self.a = 20


test_ob = Test()
print(Test.a)
print(test_ob.a)
test_ob.m1()
print('Static variable after method call :', Test.a)
print('Instance variables :', test_ob.__dict__)
print('Static variables :', Test.__dict__)
