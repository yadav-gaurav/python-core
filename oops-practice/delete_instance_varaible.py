class Student:
    """Student Info"""

    def __init__(self, name, age):
        self.name = name
        self.age = age

    def delete_instance_variable(self):
        del self.age


student_obj = Student('yadav gaurav', '12345')
# Instance variable before deleting
print(student_obj.__dict__)

# After deleting instance varibale from inside class.
student_obj.delete_instance_variable()
print(student_obj.__dict__)

# Delete Instance from outside class
del student_obj.name
print(student_obj.__dict__)
