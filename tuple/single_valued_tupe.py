# single valued tuple
# empty tuple
tp = ()
print(type(tp))

# Single valued tuple
name_tuple = ('yadav',)
print(type(name_tuple))

name_tuple2 = 'gaurav',
print(type(name_tuple2))

number_tuple = 1, 2, 3, 4,
print(type(number_tuple))

number_tuple2 = 1, 2, 3, 4
print(type(number_tuple2))

# Not tuple
number = (12)
print(type(number))

string = ('gaurav')
print(type(string))
