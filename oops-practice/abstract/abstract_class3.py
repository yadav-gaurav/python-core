from abc import ABC, abstractmethod

class R(ABC):
    def wish(self):
        print('Hey Good morning !!!')
    @abstractmethod
    def r(self):
        pass

class K(R):
    def r(self):
        super().wish()
        print(' I am implementation of abstracted method !!!')
rk = K()
rk.r()