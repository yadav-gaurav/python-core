# Create list with unique vowels in given string
input_string = input('Enter the string ')
vowels = ['a', 'e', 'i', 'o', 'u']
vowel_list = [char for char in vowels if char in input_string]
print(vowel_list)
