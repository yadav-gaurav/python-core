import gc

# check garbage collector is enabled or not.
print('status of grabage collector : ', gc.isenabled())

# disbaled
gc.disable()
print('again check status of garbage collector : ', gc.isenabled())

# enabled
gc.enable()
print('again check status of grabage collector : ', gc.isenabled())
