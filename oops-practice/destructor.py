import time


class Test:
    def __init__(self):
        print('Intializing the instance varible ....')

    # destructor method
    def __del__(self):
        print('Perform clean up activities ....')


test_obj = Test()
test_obj = None
time.sleep(10)
print('End of Application...')
