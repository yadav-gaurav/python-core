class Person:
    def __init__(self, name, age, weight):
        self.name = name
        self.age = age
        self.weight = weight
    def display(self):
        print('Name :', self.name)
        print('Age :', self.age)
        print('Weight :', self.weight)
class Employee(Person):
    def __init__(self, name, age, weight, emp_no, salary):
        super().__init__(name, age, weight)
        self.emp_no = emp_no
        self.salary = salary
    def display(self):
        super().display()
        print('Employee No :', self.emp_no)
        print('Salary :', self.salary)
        print('*'*30)
obj = Employee('Yadav Gaurav', 25, 56, 312312, 5000000)
obj.display()
obj1= Employee('Devender Yadav', 22, 78, 312312312, 900000)
obj1.display()