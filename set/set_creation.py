# create the set
# Dyanmic creation
input_set = set()
print(type(input_set))

for num in range(1, 11):
    input_set.add(num)
print(input_set)


# static creation of the set
name_set = {'yadav', 'gaurav', 'devendra'}
print('Name set :', name_set)

# string converion to set:
name_string = 'yadavgaurav'
name_string_set = set(name_string)
print('Converted String to set :', name_string_set)
