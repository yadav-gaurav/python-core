class Test:
    def m1(self, a=None, b=None, c=None):
        if a is not None and b is not None and c is not None:
            print('Three agrs')
        elif a is not None and b is not None:
            print('Two args')
        elif a is not None:
            print('one args')
        else:
            print('Zero args')

obj1 = Test()
obj1.m1(1, 2, 3)
obj1.m1(1, 2)
obj1.m1(1)
obj1.m1()