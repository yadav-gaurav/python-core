class A:
    pass


class B:
    pass


class C:
    pass


class P:
    pass


class D(A, P):
    pass


class E(B, C):
    pass


class F(D, E, C):
    pass


print('MRO of class A :', A.mro())
print('*'*30)
print('MRO of class B :', B.mro())
print('*'*30)
print('MRO of class C :', C.mro())
print('*'*30)
print('MRO of class D :', D.mro())
print('*'*30)
print('MRO of class E :', E.mro())
print('*'*30)
print('MRO of class F :', F.mro())
print('*'*30)
