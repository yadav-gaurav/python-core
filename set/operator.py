# cancatenation (+) and repetition(*) operator is not applicable to set.
print('Mathematical operator is applicable.')
s1 = {1, 2, 3}
s2 = {2, 3, 1}
print(s1 == s2)
print(s1 != s2)

# <,>=,>,=<  => Not applicable here.
s3 = {12, 34, 54}
s4 = {45, 43, 11}
print(s3 < s4, s3 <= s4)
print(s3 > s4, s3 >= s4)


# in & not in => applicable in set.


# set specific operator
# union()
s5 = {1, 2}
s6 = {3, 4}
s7 = s5.union(s6)
s8 = s5 | s6
print('Union of s5 and s6 :', s7)
print('Or operator :', s8)

# intersection()
print('intersection of s5 and s6 :', s5.intersection(s6))
print('intersection of s5 and s6 :', s5 & s6)

# difference()
print('Difference s5 and s6 :', s5.difference(s6))
print('Difference s5 and s6 :', s5-s6)


# symetric-difference()
print('Symmetric-difference s5 and s6 :', s5.symmetric_difference(s6))
print('Symmetric-difference s5 and s6 :', s5 ^ s6)
