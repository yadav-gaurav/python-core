import numpy as np 

my_arry = np.array([[1, 2, 3, 4], [5, 6, 'ram', 8]])
print('Type of my_arry : ', type(my_arry))
print('*' * 30)
print('Number of Dimensions : ', my_arry.ndim)
print('*' * 30)
print('Shape of my_arry :', my_arry.shape)
print('*' * 30)
print('Size of my_arry :', my_arry.size)
print('*' * 30)
print('Data types stored in array :', my_arry.dtype)
print('*'*30)
print(my_arry)