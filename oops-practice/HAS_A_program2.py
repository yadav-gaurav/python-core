class Car:
    def __init__(self, name, color, price, model):
        self.name = name
        self.color = color
        self.price = price
        self.model = model

    def get_info(self):
        print('Car name : {} car color : {} car price : {} car model No : {}'.format(
            self.name, self.color, self.price, self.model))


class Employee:
    def __init__(self, name, regis_no, car):
        self.name = name
        self.regis_no = regis_no
        self.car = car

    def employe_info(self):
        print('Employee name : {} Employee registartion no : {}'.format(
            self.name, self.regis_no))
        self.car.get_info()


car_obj = Car('BMW', 'black', '200000k', '3e123dqw')
employee_obj = Employee('yadav gaurav', '323232232', car_obj)
employee_obj.employe_info()
