import json
employee={'name':'yadav gaurav','age':23,'salary':260000}

#serialize python object json string.
print("seralize without any formating")
json_string=json.dumps(employee)
print(json_string)

#serlialize with sorting and preety fromate
print("With formating")
json_string_fromated=json.dumps(employee,sort_keys=True,indent=3)
print(json_string_fromated)

#deserialization json string to python object.
json_string='''{
    "name":"yadav gaurav",
    "age":23,
    "salary":230000,
    "address":"Jaunpur"
}'''

employee_details=json.loads(json_string)
print(employee_details)
print(type(employee_details))
print(employee_details['name'])
print("print using for loop .................")
for key,value in employee_details.items():
     print(key," : ",value)
print("Deseralization completed")
