import random
import time
name_list = ['yadav gaurav', 'devendera yadav',
             'ravi yadav', 'nagendra yadav', 'saroj devi']
subject_list = ['python', 'javaScript', 'java', 'c', 'c++', 'c#']
# using list
print('Using list **********************8')


def student_list(number_of_student):
    student_dict1 = []
    for i in range(number_of_student):
        student = {'id': i, 'name': random.choice(
            name_list), 'subject': random.choice(subject_list)}
        student_dict1.append(student)
    return student_dict1


t1 = time.clock()
students = student_list(1000000)
t2 = time.clock()
print('Taken time list method :', (t2-t1))


print('generator method...***************')


def student_generator(number_of_student):
    student_dict1 = []
    for i in range(number_of_student):
        student = {'id': i, 'name': random.choice(
            name_list), 'subject': random.choice(subject_list)}
        student_dict1.append(student)
    yield student_dict1


t1 = time.clock()
students = student_generator(1000000)
t2 = time.clock()
print('Taken time by generator:', (t2-t1))
