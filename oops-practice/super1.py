# overcome the naming conflict problems between parent class and child class.
class Parent:
    def m(self):
        print('I am form the parent class !!!!')


class Child(Parent):
    def m(self):
        super().m()
        print('I am from the child class...')


child_obj = Child()
child_obj.m()
