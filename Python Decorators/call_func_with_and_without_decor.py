def decorator_wish(func):
    def inner(name):
        if name == 'sunny leone':
            print('*'*10)
            print('Hey you are very important for my life pls do not go')
            print('Good morning very very to you ', name)
            print('*'*10)
        else:
            func(name)
    return inner


def wish(name):
    print('Hey Good Morning, have a nice day !!', name)


print('original function reference :', wish)
wish('Yadav Gaurav')
print('*'*20)
decorated_func = decorator_wish(wish)
print('Decorated function reference :', decorated_func)
decorated_func('sunny leone')
decorated_func('Devendera Yadav')
