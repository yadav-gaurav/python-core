import json
employee={'name':'yadav gaurav','age':23,'salary':260000}

#serialize python dict to json in file without formating.
print("serialization started..")
f=open('emp.json','w')
json.dump(employee,f,indent=2)
print("serialization completed..")

#deserialize form json file.
print("Derialization start...")
with open('emp.json','r') as f:
    employee=json.load(f)
print("derserialization completed...")
for key, value in employee.items():
    print(key," : ",value)