# aliasing
s1 = {1, 2, 3}
s2 = s1
print('Aliasing :', s2)

# cloning
s3 = s1.copy()
print('cloning of s1 :', s3)

# comprehension
number_set = {num for num in range(1, 101) if num % 2 == 0}
print(number_set)

#eg. square_set
square_set = {2**num for num in range(1, 11)}
print('square_set :', square_set)
