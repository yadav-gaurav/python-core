import pickle
class Employee:
    """Intialize employee detalis & Display Employee detalis"""
    def __init__(self,name,age,salary,address):
        self.name=name
        self.age=age
        self.salary=salary
        self.address=address

    def display(self):
        print('Employee name: {}, Employee age: {}, Employee Salary: {}, Employee address: {}'.format(self.name,self.age,self.salary,self.address))
emp_obj=Employee('yadav',23,26000,'Jaunpur')

#serialization
print("Serializations start ....")
with open('emp.ser','wb') as f:
    pickle.dump(emp_obj,f)
print("Serialization completed")
#deserialization
print("Deserialization start..")
with open('emp.ser','rb') as f:
    emp_details=pickle.load(f)
print("Deserialization completed ...")
emp_obj.display()