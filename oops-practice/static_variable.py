class Student:
    """Student information intialization and diplay"""
    school_name = 'B N B I College'
    address = 'Meerpur Belwa Bazar'

    def __init__(self, name, roll_number):
        self.name = name
        self.roll_number = roll_number
        Student.num = 20

    @classmethod
    def add_data(cls):
        cls.a = 10
        Student.b = 23

    @staticmethod
    def intialize_static_variable():
        Student.c = 34


# Static varible before object creation
print('Static varible before object creation :', Student.__dict__)

# Static variable after objecte
stud_ob = Student('yadav gaurav', 256)
print('Static variable after objecte :', Student.__dict__)

# Static variable after method call
Student.intialize_static_variable()
print('Static variable after method call :', Student.__dict__)
