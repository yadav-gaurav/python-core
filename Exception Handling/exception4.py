try:
    x = int(input('Enter the value of x :'))
    y = int(input('Enter the value of y :'))
    print('Result is :', x / y)
except ZeroDivisionError:
    print('Can not divide the x by y')
except ValueError:
    print('Please provide the proper input !!')