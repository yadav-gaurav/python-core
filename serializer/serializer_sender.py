import pickle
from employee_details import Employee

f=open('emp1.ser','wb')

# serialization
while True:
     name=input("Enter Employee name :")
     age=int(input("Enter Employee age :"))
     salary=int(input("Enter employee salary :"))
     emp_ob=Employee(name,age,salary)
     pickle.dump(emp_ob,f)
     option=input("Do you want to serialize another object [yes/no]")
     if option.lower()=='no':
         break
print("Serialization completed.....")