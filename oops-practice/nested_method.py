import gc


class Test:
    def m1(self):
        def calc(a, b):
            print('Addition is : ', a+b)
            print('Subtraction is : ', a-b)
            print('Multiplication is : ', a*b)
            print('Division is : ', a/b)

        calc(10, 2)
        calc(100, 20)
        calc(200, 5)


test_obj = Test()
test_obj.m1()

# check garabge collector is enabled is or not.
print('check garbage collector status : ', gc.isenabled())
