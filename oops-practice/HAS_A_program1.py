# HAS-A realtionship is called as composition
class Engine:
    def m(self):
        print('Engine realted functionality...')


class Car:
    def __init__(self):
        self.engine_ob = Engine()

    def m1(self):
        print('perform car functionality...')
        self.engine_ob.m()


car_ob = Car()
car_ob.m1()
