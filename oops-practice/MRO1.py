# method resolution (MRO)
class A:
    pass


class B(A):
    pass


class C(A):
    pass


class D(B, C):
    pass


print('MRO of class A', A.mro())
print('MRO of class B', B.mro())
print('MRO of class C', C.mro())
print('MRO of class D', D.mro())
