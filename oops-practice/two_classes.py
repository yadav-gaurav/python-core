class Employee:
    def __init__(self, name, designation, salary):
        self.name = name
        self.designation = designation
        self.salary = salary

    def get_employee_details(self):
        print("Employee Information....")
        print('Employee Name : ', self.name)
        print('Employee Designation : ', self.designation)
        print('Employee Salary : ', self.salary)


class Manager:
    def update_employee_info(emp_obj):
        emp_obj.salary = emp_obj.salary+10000
        emp_obj.get_employee_details()


emp_obj = Employee('Yadav Gaurav', 'Software Developer', 26000)
Manager.update_employee_info(emp_obj)
