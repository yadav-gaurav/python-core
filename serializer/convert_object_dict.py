class Employee:
    """Serialize multiple employee objects"""
    def __init__(self, name, age, salary):
        self.name = name
        self.age = age
        self.salary = salary
        
    def display(self):
        print('Employee name: {}, Employee age: {}, Employee salary: {}'.format(self.name,self.age,self.salary))


emp_obj = Employee('yadav', 25, 200000)

# convert object to dict using method:
print("converison using __dict__ method")
emp_dict = emp_obj.__dict__
print(emp_dict)

# convert object to dict manually
print("conversion using manauallly approach")
empoyee_dict = {'name': emp_obj.name, 'age': emp_obj.age, 'salary': emp_obj.salary}
print(empoyee_dict)