# call particular method from the parent class.
# using two ways
# classname.method(self)
class A:
    def m(self):
        print('Class A method')


class B(A):
    def m(self):
        print('Class B method')


class C(B):
    def m(self):
        print('Class C method')


class D(C):
    def m(self):
        print('Class D method')


class E(D):
    def m(self):
        super(B, self).m()
        # A.m(self)


e_obj = E()
e_obj.m()
